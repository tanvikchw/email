package email1

import grails.test.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class SendMailServiceSpec extends Specification {

    SendMailService sendMailService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new SendMail(...).save(flush: true, failOnError: true)
        //new SendMail(...).save(flush: true, failOnError: true)
        //SendMail sendMail = new SendMail(...).save(flush: true, failOnError: true)
        //new SendMail(...).save(flush: true, failOnError: true)
        //new SendMail(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //sendMail.id
    }

    void "test get"() {
        setupData()

        expect:
        sendMailService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<SendMail> sendMailList = sendMailService.list(max: 2, offset: 2)

        then:
        sendMailList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        sendMailService.count() == 5
    }

    void "test delete"() {
        Long sendMailId = setupData()

        expect:
        sendMailService.count() == 5

        when:
        sendMailService.delete(sendMailId)
        sessionFactory.currentSession.flush()

        then:
        sendMailService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        SendMail sendMail = new SendMail()
        sendMailService.save(sendMail)

        then:
        sendMail.id != null
    }
}
