package email1

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
@Secured(['ROLE_USER','ROLE_ADMIN'])

class SendMailController {

    SendMailService sendMailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def send() {
        def  multipartFile = request.getFile('attachment')
        sendMail {
            multipart true
            to params.address
            subject params.subject
            html params.body
            if(multipartFile && !multipartFile.empty)
            {
                File tmpFile = new File(System.getProperty("java.io.tmpdir")+System.getProperty("file.seperator")+multipartFile.getOriginalFilename());
                multipartFile.transferTo(tmpFile);
                attach tmpFile
            }
        }

        flash.message = "Message sent at "+new Date()
        redirect action:"index"
    }
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond sendMailService.list(params), model:[sendMailCount: sendMailService.count()]
    }

    def show(Long id) {
        respond sendMailService.get(id)
    }

    def create() {
        respond new SendMail(params)
    }

    def save(SendMail sendMail) {
        if (sendMail == null) {
            notFound()
            return
        }

        try {
            sendMailService.save(sendMail)
        } catch (ValidationException e) {
            respond sendMail.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'sendMail.label', default: 'SendMail'), sendMail.id])
                redirect sendMail
            }
            '*' { respond sendMail, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond sendMailService.get(id)
    }

    def update(SendMail sendMail) {
        if (sendMail == null) {
            notFound()
            return
        }

        try {
            sendMailService.save(sendMail)
        } catch (ValidationException e) {
            respond sendMail.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'sendMail.label', default: 'SendMail'), sendMail.id])
                redirect sendMail
            }
            '*'{ respond sendMail, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        sendMailService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'sendMail.label', default: 'SendMail'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'sendMail.label', default: 'SendMail'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
