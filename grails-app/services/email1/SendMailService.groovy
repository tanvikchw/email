package email1

import grails.gorm.services.Service

@Service(SendMail)
interface SendMailService {

    SendMail get(Serializable id)

    List<SendMail> list(Map args)

    Long count()

    void delete(Serializable id)

    SendMail save(SendMail sendMail)

}